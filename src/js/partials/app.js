$(window).on('load', function() {
	setTimeout(() => {
		$('body').removeClass('is-preload');
	}, 1000);
});

$('document').ready(function() {
	init();
});

$(window).resize(function () {
	init();
});

function init() {

	const $window = $(window);
	const $body = $('body');
	const $header = $('.header');
	const $hamburger = $('.js-hamburger');
	const $nav = $('.nav');
	const $navLink = $nav.find('.nav__link');
	const $navLinkMob = $('.mob-menu .nav .nav__link');

	if ($window.width() > 991) {

		/*START AOS JS*/
		AOS.init({
			easing: 'ease-out-back',
			duration: 1000,
			disable: 'mobile',
			once: true
		});
		/*END AOS JS*/


		$navLink.on('click', function (e) {
			e.preventDefault();

			const $this = $(this);
			// let index = $this.data('index');

			// if (!$this.hasClass('active')) {
			// 	$navLink.removeClass('active');
			// 	$this.addClass('active');
			//
			// 	if ($body.hasClass('menu-open') && $hamburger.hasClass('is-active')) {
			// 		$hamburger.removeClass('is-active');
			// 		$body.removeClass('menu-open');
			// 	}
			// }

			$navLink.removeClass('active');
			$this.addClass('active');

			const anchor = $(this);
			let ancAtt = $(anchor.attr('href'));

			$('html, body').stop().animate({
				scrollTop: ancAtt.offset().top - $header.outerHeight()
			}, 1000);

		});

	}

	if ($window.width() < 992) {
		$navLinkMob.on('click', function (e) {
			e.preventDefault();

			const anchor = $(this);
			let ancAtt = $(anchor.attr('href'));

			$('html, body').stop().animate({
				scrollTop: ancAtt.offset().top - $header.outerHeight()
			}, 1000);

			$hamburger.removeClass('is-active');
			$body.removeClass('menu-open');
		});

		AOS.init({
			easing: 'ease-out-back',
			duration: 1000,
			once: true
		});
	}


	if ($hamburger.length) {
		$hamburger.on('click', function () {
			$(this).toggleClass('is-active');
			$body.toggleClass('menu-open');
		})
	}

	$(window).scroll(function (e) {

		if ($(document).scrollTop() > 10) {
			$header.addClass('sticky')
		} else {
			$header.removeClass('sticky')
		}

	});
}
